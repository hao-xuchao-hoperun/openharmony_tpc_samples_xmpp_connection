## ml-array-max单元测试用例

该测试用例基于OpenHarmony系统下，参照[原库测试用例](https://github.com/mljs/array/tree/master/packages/array-max/src/__tests__/max.test.js) 进行单元测试

### 单元测试用例覆盖情况

| 接口名                                                                 | 是否通过 |备注|
|---------------------------------------------------------------------|---|---|
| max(array: ArrayLike<number>, options?: mlArrayMax.ArrayMaxOptions) |pass|