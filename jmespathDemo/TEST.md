# jmesPathDemo单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/jmespath/jmespath.js/tree/master/test)
进行单元测试

单元测试用例覆盖情况

|                 接口名                 |是否通过	|备注|
|:-----------------------------------:|:---:|:---:|
|             tokenize()              |    pass        |       |
|              parsing()              |pass   |        |
|          strictDeepEqual()          |pass   |        |
|              search()               |pass   |        |
|               basic()               |pass   |        |
|              boolean()              |pass   |        |
|              current()              |pass  |     |
|              escape()               |   pass  |          |
|              filters()              | pass |  |
|             functions()             | pass  |       |
|            identifiers()            |  pass |          |
|              indices()              |  pass |          |
|              literal()              | pass  |          |
|            multiselect()            | pass  |          |
|               pipe()                |  pass |          |
|               slice()               |  pass |          |
|              syntax()               | pass  |          |
|              unicode()              | pass  |          |
|              wildcard()              | pass  |          |
