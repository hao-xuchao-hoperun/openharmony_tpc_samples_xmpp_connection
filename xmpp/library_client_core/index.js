"use strict";


import { Client } from "./src/main/client-core/lib/Client"

import xml from "@xmpp/xml"

import { jid } from "@xmpp/jid"

export { xml, jid, Client }
