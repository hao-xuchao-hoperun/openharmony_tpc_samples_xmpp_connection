import libxmljs from "./parsers/libxmljs";
import ltx from "./parsers/ltx";
// import expat from "./parsers/node-expat.js";
import xml from "./parsers/node-xml";
import saxjs from "./parsers/sax-js";
import saxes from "./parsers/saxes";

export default [
  libxmljs,
  ltx,
  // expat,
  xml,
  saxjs,
  saxes,
];
