# Changelog

## [1.0.0] 
- SASL ANONYMOUS 机制：它提供了 RFC 7525 中定义的 ANONYMOUS SASL机制的实现，这是XMPP协议的一个扩展。
- XMPP 客户端集成：它可以集成到 XMPP 客户端库或应用程序中，以添加对匿名认证的支持。
- 客户端与服务器认证：XMPP 客户端能够使用 ANONYMOUS 机制与 XMPP 服务器进行认证，该机制不需要客户端提供用户名或密码。
