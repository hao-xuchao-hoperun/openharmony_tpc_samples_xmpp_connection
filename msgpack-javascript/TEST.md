## msgpack-javascript单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/msgpack/msgpack-javascript/tree/v2.8.0/test) 进行单元测试

### 单元测试用例覆盖情况

|接口名 | 是否通过 |备注|
|---|---|---|
|encode|pass||
|decode|pass||
|decodeMulti|pass||
|DecodeOptions|pass||
|decodeAsync|pass||
|decodeArrayStream|pass||
|decodeMultiStream|pass||
|decodeStream|pass||
|Encoder|pass||
|Decoder|pass||
|ExtensionCodec|pass||
|ExtData|pass||
|EXT_TIMESTAMP|pass||
|encodeDateToTimeSpec|pass||
|encodeTimeSpecToTimestamp|pass||
|decodeTimestampToTimeSpec|pass||
|encodeTimestampExtension|pass||
|decodeTimestampExtension|pass||