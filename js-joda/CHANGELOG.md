## 2.0.0

- DevEco Studio 版本： 4.1 Canary(4.1.3.317),OpenHarmony SDK:API11 (4.1.0.36)
- ArkTs语法适配
- 将local_en-us改为本地依赖

## 1.0.1
适配DevEco Studio 3.1 Beta1版本

## v1.0.0
适配兼容OpenHarmony系统的一款不可变时间日期开源库。
* 轻量级，压缩后只有43kb,没有三方依赖
* 速度很快，比其他的JavaScript日期库快2到10倍
* 健壮和稳定，经过大量测试用例验证
