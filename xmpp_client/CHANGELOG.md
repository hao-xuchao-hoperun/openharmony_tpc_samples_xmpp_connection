# Changelog

## 1.0.2
- 新增tls、starttls、xmpp_sasl_scram_sha_1使用逻辑
- 新增sample中tcp、tls使用场景

## 1.0.1
- 修复websocket包内crash问题

## 1.0.0
- 完成xmpp TCP连接、断连、重连功能。
- 完成通信功能。
- 完成状态（在线、离线）监听。
- 完成错误、连接、断连的回调功能 。
- 更新说明文档。

## 遗留问题
- tls连接（证书校验）。
- websocket连接（证书校验）。
- 在tls、websocket下通信还需证书校验后完成。

