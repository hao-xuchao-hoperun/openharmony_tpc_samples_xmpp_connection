# Changelog

## 1.0.0
- 安全认证：提供了基于 SCRAM（Salted Challenge Response Authentication Mechanism）的安全认证机制，确保用户在 XMPP 通信中的身份验证安全。
- 哈希加密：该模块使用 SHA-1 哈希算法对用户密码进行加密处理，增强了密码传输过程中的安全性，防止密码在传输过程中被截获和破解。



