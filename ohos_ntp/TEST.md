# ohos_ntp单元测试用例

该测试用例基于OpenHarmony系统下，进行单元测试

单元测试用例覆盖情况

|    库名     |               接口名               |    是否通过	     |备注|
|:---------:|:-------------------------------:|:------------:|:---:|
| ohos_ntp | {server,port,delayTime}:NTPConfig | pass |       |