# Changelog

## 1.0.0
- TLS (Transport Layer Security) 握手：在 XMPP 连接中启用 TLS 加密，以确保安全的通信。
- 自动 STARTTLS：自动检测服务器是否支持 StartTLS，以进行有条件地安全连接。
- 错误处理：捕捉和处理 StartTLS 过程中可能发生的错误和异常。




