# Changelog

## 1.0.0
- 数据加密：可以对XMPP协议中传输的数据进行加密，确保消息传输过程中的数据安全和隐私。
- 身份验证：可以对服务器和客户端的身份进行验证，确保连接到的是真正的服务器和客户端。
- 自动证书生成和管理：XMPP协议支持使用STARTTLS命令来明确启动TLS会话，并支持使用SSL/TLS证书的自动生成和管理。
- 安全通信：TLS加密可以确保XMPP协议中的消息传输和通信的安全性，避免消息被窃听或篡改。




