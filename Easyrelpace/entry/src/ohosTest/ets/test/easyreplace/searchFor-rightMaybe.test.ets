/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import hilog from '@ohos.hilog';
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium'
import { er, Opts } from "easy-replace";

export default function searchForrightMaybe() {
  describe('searchForrightMaybe', () => {
    // Defines a test suite. Two parameters are supported: test suite name and test suite function.
    beforeAll(() => {
      // Presets an action, which is performed only once before all test cases of the test suite start.
      // This API supports only one parameter: preset action function.
    })
    beforeEach(() => {
      // Presets an action, which is performed before each unit test case starts.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: preset action function.
    })
    afterEach(() => {
      // Presets a clear action, which is performed after each unit test case ends.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: clear action function.
    })
    afterAll(() => {
      // Presets a clear action, which is performed after all test cases of the test suite end.
      // This API supports only one parameter: clear action function.
    })
    // it('assertContain',0, function () {
    //   // Defines a test case. This API supports three parameters: test case name, filter parameter, and test case function.
    //   hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.INFO);
    //   hilog.info(0x0000, 'testTag', '%{public}s', 'it begin');
    //   let a = 'abc'
    //   let b = 'b'
    //   // Defines a variety of assertion methods, which are used to declare expected boolean conditions.
    //   expect(a).assertContain(b)
    //   expect(a).assertEqual(a)
    // })

    let count = 0;
    let test = (name: string, func: Function) => {
      name = name.replace(new RegExp("/[ /d]/g"), '');
      name = name.replace(new RegExp("/-/g"), "");
      it(name, count++, func)
    }

    let equal = (src: string, dst: string, tag: string) => {
      console.log('tag:' + tag + "  src=" + src + " dst=" + dst);
      expect(src).assertEqual(dst)
    }

    // ==============================
    // searchFor + rightMaybe
    // ==============================
    let optsFunc = (leftOutsideNot: string | string[] = "",
                    leftOutside: string | string[] = "",
                    leftMaybe: string | string[] = "",
                    searchFor: string | string[] = "",
                    rightMaybe: string | string[] = "",
                    rightOutside: string | string[] = "",
                    rightOutsideNot: string | string[] = "",rightMaybeBool:boolean=false): Opts => {
      return {
        leftOutsideNot,
        leftOutside,
        leftMaybe,
        searchFor,
        rightMaybe,
        rightOutside,
        rightOutsideNot,
        i: {
          leftOutsideNot: false,
          leftOutside: false,
          leftMaybe: false,
          searchFor: false,
          rightMaybe: rightMaybeBool,
          rightOutside: false,
          rightOutsideNot: false
        }
      }
    }
    test("01 - right maybe found", () => {
      equal(
        er(
          "a🦄🐴🦄c",optsFunc("","","","🐴","🦄","",""), "b"
        ),
        "a🦄bc",
        "test 3.1.1"
      );
      equal(
        er(
          "a🦄🐴🦄c",optsFunc("","","","🐴",["🦄"],"",""), "b"
        ),
        "a🦄bc",
        "test 3.1.2"
      );
    });

    test("02 - two replacements with one rightmaybe, nearby", () => {
      equal(
        er(
          "ab🐴🦄🐴c",optsFunc("","","","🐴","🦄","",""), "d"
        ),
        "abddc",
        "test 3.2.1"
      );
      equal(
        er(
          "ab🐴🦄🐴c",optsFunc("","","","🐴",["🦄"],"",""), "d"
        ),
        "abddc",
        "test 3.2.2"
      );
    });

    test("03 - two consecutive right maybes", () => {
      equal(
        er(
          "ab🦄🐴🦄🐴c",optsFunc("","","","🦄","🐴","",""), "d"
        ),
        "abddc",
        "test 3.3.1"
      );
      equal(
        er(
          "ab🦄🐴🦄🐴c",optsFunc("","","","🦄",["🐴"],"",""),"d"
        ),
        "abddc",
        "test 3.3.2"
      );
    });

    test("04 - futile right maybe", () => {
      equal(
        er("'🐴",optsFunc("","","","🐴","🦄","",""),"d"),
        "'d",
        "test 3.4.1"
      );
      equal(
        er(
          "'🐴",optsFunc("","","","🐴",["🦄"],"",""), "d"
        ),
        "'d",
        "test 3.4.2"
      );
    });

    test("05 - \\n as search string plus right maybe", () => {
      equal(
        er(
          "\na\n\n",optsFunc("","","","\n","a","",""), "a"
        ),
        "aaa",
        "test 3.5.1"
      );
      equal(
        er(
          "\na\n\n",optsFunc("","","","\n",["a"],"",""), "a"
        ),
        "aaa",
        "test 3.5.2"
      );
    });

    test("06 - \\n as both searchFor and right maybe, replaced", () => {
      equal(
        er(
          "\n\n\n",optsFunc("","","","\n","\n","",""), "a"
        ),
        "aa",
        "test 3.6.1"
      );
      equal(
        er(
          "\n\n\n",optsFunc("","","","\n",["\n"],"",""), "a"
        ),
        "aa",
        "test 3.6.2"
      );
    });

    test("07 - rightMaybe with line breaks", () => {
      equal(
        er(
          "a\n\na",optsFunc("","","","a","\n\na","",""), "b"
        ),
        "b",
        "test 3.7.1"
      );
      equal(
        er(
          "a\n\na",optsFunc("","","","a",["\n\na"],"",""), "b"
        ),
        "b",
        "test 3.7.2"
      );
    });

    test("08 - specific case of semi infinite loop with maybe", () => {
      equal(
        er(
          "aaaaab",optsFunc("","","","a","b","",""), "a"
        ),
        "aaaaa",
        "test 3.8.1"
      );
      equal(
        er(
          "aaaaab",optsFunc("","","","a",["b"],"",""), "a"
        ),
        "aaaaa",
        "test 3.8.2"
      );
    });

    test("09 - three right maybes (some found)", () => {
      equal(
        er(
          "a🦄🐴🦄c",optsFunc("","","","🐴",["x", "c", "🦄"],"",""), "b"
        ),
        "a🦄bc",
        "test 3.9"
      );
    });

    test("10 - three right maybes (searchFor not found)", () => {
      equal(
        er(
          "a🦄🐴🦄c",optsFunc("","","","🍺",["🦄", "🐴", "c"],"",""), "b"
        ),
        "a🦄🐴🦄c",
        "test 3.10"
      );
    });

    test("11 - three right maybes (maybes not found)", () => {
      equal(
        er(
          "🍺🦄🐴🦄c",optsFunc("","","","🍺",["x", "y", "z"],"",""), "1"
        ),
        "1🦄🐴🦄c",
        "test 3.11"
      );
    });

    test("12 - three right maybes (multiple hungry finds)", () => {
      equal(
        er(
          "🐴 ",optsFunc("","","","🐴",["🦄", "🍺", "c"],"",""), "b"
        ),
        "b ",
        "test 3.12.1"
      );
    });

    test("13 - three right maybes (multiple hungry finds)", () => {
      equal(
        er(
          "a🦄🐴🦄🍺c",optsFunc("","","","🐴",["🦄", "🍺", "c"],"",""), "b"
        ),
        "a🦄b🍺c",
        "test 3.13"
      );
    });

    test("14 - three right maybes (multiple hungry finds)", () => {
      equal(
        er(
          "a🦄🐴🍺🦄c",optsFunc("","","","🐴",["🦄", "🍺", "c"],"",""), "b"
        ),
        "a🦄b🦄c",
        "test 3.14"
      );
    });

    test("15 - three right maybes (multiple hungry finds)", () => {
      equal(
        er(
          "a🦄🐴🦄c",optsFunc("","","","🐴",["🦄", "🍺", "c"],"",""), "b"
        ),
        "a🦄bc",
        "test 3.15"
      );
    });

    test("16 - three right maybes (multiple hungry finds)", () => {
      equal(
        er(
          "a🦄🐴🦄🍺c a🦄🐴🍺🦄c a🦄🐴🦄c 🐴",optsFunc("","","","🐴",["🦄", "🍺", "c"],"",""), "b"
        ),
        "a🦄b🍺c a🦄b🦄c a🦄bc b",
        "test 3.16"
      );
    });

    test("17 - three right maybes (multiple hungry finds)", () => {
      equal(
        er(
          "🦄y🦄 🦄y🦄 🦄y🦄 y",optsFunc("","","","y",["🦄"],"",""),"b"
        ),
        "🦄b 🦄b 🦄b b",
        "test 3.17"
      );
    });

    test("18 - three right maybes (multiple hungry finds)", () => {
      equal(
        er(
          "🦄y🦄 🦄y🦄 🦄y🦄 y",optsFunc("","","","y","🦄","",""), "b"
        ),
        "🦄b 🦄b 🦄b b",
        "test 3.18"
      );
    });
    // if leftMaybe is simply merged and not iterated, and is queried to exist
    // explicitly as string on the right side of the searchFor, it will not be
    // found if the order of array is wrong, yet characters are all the same.

    test("19 - sneaky array conversion situation", () => {
      equal(
        er(
          "a🦄🐴🦄c",optsFunc("","","","🐴",["c","🦄"],"",""), "b"
        ),
        "a🦄bc",
        "test 3.19-1"
      );
      equal(
        er(
          "a🦄🐴🦄c",optsFunc("","","","🐴",["🦄","c"],"",""), "b"
        ),
        "a🦄bc",
        "test 3.19-2"
      );
    });

    test("20 - normal words, few of them, rightMaybe as array", () => {
      equal(
        er(
          "this protection is promoting the proper propaganda",optsFunc("","","","pro",["tection", "mot", "p", "paganda"],"",""), "test"
        ),
        "this test is testing the tester test",
        "test 3.20"
      );
    });

    test("21 - rightMaybe is array, but with only 1 null value", () => {
      equal(
        er(
          "some text",optsFunc("","","","look for me",[""],"",""), "replace with me"
        ),
        "some text",
        "test 3.21"
      );
    });

    test("22 - rightMaybe is couple integers in an array", () => {
      equal(
        er(
          "1234",optsFunc("","","","2",["3","4"],"",""), "9"
        ),
        "194",
        "test 3.22"
      );
    });

    test("23 - sneaky case of overlapping rightMaybes", () => {
      equal(
        er(
          "this is a word to be searched for",optsFunc("","","","word",[" to", " to be", "word to be"],"",""), "x"
        ),
        "this is a x searched for",
        "test 3.23"
      );
    });

    test("24 - case-insensitive flag", () => {
      equal(
        er(
          "aaaC",optsFunc("","","","aaa",["x", "y", "z"],"","",true), "x"
        ),
        "xC",
        "test 3.24"
      );
    });

  })
}
