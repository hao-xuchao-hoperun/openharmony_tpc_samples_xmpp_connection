## js-diff单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/kpdecker/jsdiff/tree/master/test) 进行单元测试

### 单元测试用例覆盖情况

|接口名 | 是否通过 |备注|
|---|---|---|
|diffChars|pass|
|diffWords|pass|
|diffWordsWithSpace|pass|
|diffLines|pass|
|diffTrimmedLines|pass|
|diffSentences|pass|
|diffCss|pass|
|diffJson|pass|
|diffArrays|pass|
|createPatch|pass|
|createTwoFilesPatch|pass|
|structuredPatch|pass|
|parsePatch|pass|
|convertChangesToXML|pass|
|canonicalize|pass|
|merge|pass|
|applyPatch|pass|
|applyPatches|pass|