## v2.0.0

- 适配DevEco Studio 版本： 4.1 Canary(4.1.3.317), OpenHarmony SDK:API11 (4.1.0.36)
- 支持效果点击或拖动的动态交互实现

## 1.0.0

- 详细功能：
  1. 形状（shape）以及形状的定义
  2. 刚体（body）以及刚体的定义
  3. 碰撞体（fixture）以及碰撞体的定义
  4. 约束(constraint)的定义和实现
  5. 关节（joint）的定义实现
  6. 接触约束（contact constraint）定义实现
  7. 世界（world）定义实现
  8. 求解器（solver）定义实现
  9. 连续式碰撞(continuous collision)