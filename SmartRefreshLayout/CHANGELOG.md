## 2.1.0-rc.0

- 适配V2装饰器
## 2.0.1

- 发布2.0.1正式版本

## 2.0.1-rc.0

- 修复resource目录下资源文件引用不到的问题

## 2.0.0

- DevEco Studio 版本： 4.1 Canary(4.1.3.317),OpenHarmony SDK:API11 (4.1.0.36)
- ArkTs语法整改

## 1.0.2

- 优化库的接口，补充一些对外属性
- 添加加载动画的游戏功能

## 1.0.1

- api8升级到api9

## 1.0.0

实现下拉头部刷新相关基本操作
1. Delivery样式刷新
2. DropBox样式刷新
3. BezierRadar样式刷新
4. BezierCircle样式刷新
5. FlyRefresh样式刷新
6. Classics样式刷新
7. Phoenix样式刷新
8. Taurus样式刷新
9. BattleCity样式刷新
10. HitBlock样式刷新
11. WaveSwipe样式刷新
12. Material样式刷新
13. StoreHouse样式刷新
14. WaterDrop样式刷新