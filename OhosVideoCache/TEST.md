## OhosVideoCache单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/danikula/AndroidVideoCache/tree/master/test/src/test) 进行单元测试

### 单元测试用例覆盖情况

|接口名 | 是否通过 |备注|
|---|---|---|
|getProxyUrl|pass||
|registerCacheListener|pass||
|unregisterCacheListener|pass||
|shutdown|pass||
|cacheDirectory|pass||
|setFileNameGenerator|pass||
|maxCacheSize|pass||
|maxCacheFilesCount|pass||
|setDiskUsage|pass||
|setHeaderInjector|pass||
|build|pass||

