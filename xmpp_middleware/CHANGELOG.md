# Changelog

## 1.0.0
- 实现中间件的注册、应用和处理。
- 实现消息处理,发送和接收过程中应用中间件。
- 实现事件处理,XMPP事件发生时应用中间件。
