// C++ wrapper for LuaJIT header files.
#ifndef LUA_HAPP
#define LUA_HAPP

extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
#include "luajit.h"
}

#endif

